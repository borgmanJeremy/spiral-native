Spiral Changelog:

Version Release 1.3:
*  Links in textboxes are now clickable
*  Added font selector (Thank you, https://gitlab.com/DrewTechs)
*  "Type here" text is now highlighted by default on box creation
*  Added icon for "insert image" button

Version Release 1.2:
*  Greatly improved average memory usage by having notebooks load pages when accessed individually instead of pre-loading all pages in the notebook on open
*  Added splitter to main view so notebook/section browser can be resized by the user
*  Improved text find behavior that caused it to start over at the first instance of a query if the dialog had been closed and re-opened without changing the query
*  Fixed a bug that caused some textboxes to occasionally be shorter than they should have been until being clicked
*  Patched all known memory leaks (some text find leaks had gone unnoticed by me in previous tests - oops)
*  More bug fixes

Version Release 1.1:
*  Added ability to rearrange pages by dragging
*  Added quick highlight feature, which applies the last highlight color used in the current TextBox to the text under the cursor when you press CTRL+H
*  Fixed a bug that caused pages with a page size greater than the default to be slightly cut off until re-dragging the furthest TextBoxes
*  Bug Fixes

Version Release 1.0:
*  Starting in this version, whenever a Spiral notebook is opened, a copy of that notebook is saved on open in the same directory as [notebook name].bak in case a crash causes any data loss
*  Added error handling for malformed .snb files and other common user errors
*  Added automatic file metadata correction for erroneous UUIDs
*  Added crash detection and recovery instructions
*  Spiral is now maximized by default

Version Beta 1.2:
*  Added find dialog (CTRL+F)
*  Fixed a bug that caused selections in multiple TextBoxes to be allowed concurrently

Version Beta 1.1:
*  Autosaving is now multithreaded for improved performance
*  Autosave can now be disabled in the Tools menu
*  Fixed a bug that caused HTML from one textbox to be added to any textbox it is dragged into
*  Toolbar is now disabled after textbox deletion until another textbox is focused
*  Textboxes now autofocus on creation
*  Added border to textboxes so draggable region is more easily visible

Version Beta 1.0:
*  First public beta release of Spiral native for Linux and Windows.
